---
layout: note
tags:
  - tech
  - individual
---

# Linus Torvalds

Linus Torvalds is one of the world's most respected geniuses.

https://youtu.be/i2lhwb_OckQ

He is the creator of [[Linux]] and [[git]], two of the world's most impactful [[opensource]] projects.
