---
layout: note
tags:
  - tech
  - individual
  - business
  - life
---

# Naval Ravikant

When Naval Ravikant talks, you can't not listen. He runs interesting tech
investment companies and talks about [[life]], [[entrepreneurship]] and [[money]].

Website: https://nav.al

Twitter: https://twitter.com/naval

Companies:

 - https://spearhead.co
 - https://joingalaxy.co

TODO: Link favourite talks.
