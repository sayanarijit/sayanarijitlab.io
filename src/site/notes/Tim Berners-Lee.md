---
layout: note
tags:
  - tech
  - individual
  - web
---

# Tim Berners-Lee

Sir Tim Berners-Lee invented the World Wide Web in 1989.

He blogs, talks about web and is currently involved with [[Decentralization]] of the web.

Website: https://www.w3.org/People/Berners-Lee/

---

Also See:

- https://solidproject.org/
