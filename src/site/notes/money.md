---
layout: note
tags:
  - career
  - life
  - business
---

# Money

I believe in earning more as opposed of saving more, but not at the cost of losing what defines me.

## Resources

### Personal Budget

#### YNAB Method

Just read the rules even if you don't use the software. You will become a better human being.

- https://www.youneedabudget.com/the-four-rules/

#### Creating Wealth

- by Naval: https://youtu.be/1-TZqOsVCNM (ignore the catchy title)

#### Real Estate Basics

- https://subtlere.notion.site/subtlere/Real-Estate-Basics-subtlerealestate-com-14738fecd1dc4acd9fcb805641fe5a1a
