---
layout: note
tags:
  - tech
  - individual
  - computer-science
  - learning
  - rust
---

# David Pedersen

Developer and maintainer of the axum web framework[^1]. He does live coding
and youtube videos on educational and real world projects.

- GitHub: https://github.com/davidpdrsn
- Twitter: https://twitter.com/davidpdrsn
- Youtube: https://www.youtube.com/channel/UCDmSWx6SK0zCU2NqPJ0VmDQ
- Twitch: https://www.twitch.tv/davidpdrsn

A lot to learn there.

[^1]: https://github.com/tokio-rs/axum
