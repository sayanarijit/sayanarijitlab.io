---
layout: note
tags:
  - tech
  - individual
  - computer-science
  - learning
  - rust
---

# David Tolnay

David Tolnay has made tremendous contributions[^1] towards [[Rust]] and its ecosystem.

You can learn a lot about Rust from his GitHub projects and discussions on social media.

GitHub: https://github.com/dtolnay

Twitter: https://twitter.com/davidtolnay

Reddit: https://www.reddit.com/user/dtolnay


[^1]: https://www.reddit.com/r/rust/comments/mify2o/david_tolnay_thank_you/
