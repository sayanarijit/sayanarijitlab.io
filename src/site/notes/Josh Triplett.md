---
layout: note
tags:
  - tech
  - individual
  - computer-science
  - rust
---

# Josh Triplett

Josh Triplett is a developer and co-leads the [[Rust]] team. From building the dream build system, to launching [[Linux]] powered rockets, his portfolio can inspire anyone into [[software development]].

He writes about [[WebAssembly]], [[Rust]], [[Operating System]] (specifically [[Linux]] based) and [[git]].

Website: https://joshtriplett.org/

Twitter: https://twitter.com/josh_triplett

GitHub: https://github.com/joshtriplett
