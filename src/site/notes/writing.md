---
layout: note
tags:
  - learning
  - writing
  - career
---

# Writing

## Technical

### LEADERSHIP LAB: The Craft of Writing Effectively

https://youtu.be/vtIzMaLkCaM

### LEADERSHIP LAB: Writing Beyond the Academy 1.23.15

https://youtu.be/aFwVf5a3pZM

## Non Technical

### Fiction

See [[fiction]].

---

Also see:

- [[speaking]]
- https://supermemo.guru/wiki/Advantages_of_incremental_writing
- https://news.ycombinator.com/item?id=21691168
