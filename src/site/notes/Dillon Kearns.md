---
layout: note
tags:
  - individual
  - computer-science
  - learning
  - elm
---

# Dillon Kearns

He is a trainer of [[Elm]]. He has contributed many [[opensource]] libraries for the [[Elm]] ecosystem. He also writes about Elm at https://incrementalelm.com and talks about Elm at https://elm-radio.com/.

Website: https://dillonkearns.com/ and https://incrementalelm.com

GitHub: https://github.com/dillonkearns/

Twitter: https://twitter.com/dillontkearns
