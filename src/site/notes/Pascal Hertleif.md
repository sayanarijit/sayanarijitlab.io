---
layout: note
tags:
  - tech
  - individual
  - computer-science
  - learning
  - rust
---

# Pascal Hertleif

Pascal Hertleif contributes to [[Rust]] by publishing projects and talks.

Website: https://pascalhertleif.de

GitHub: https://github.com/killercup

Twitter: https://twitter.com/killercup
