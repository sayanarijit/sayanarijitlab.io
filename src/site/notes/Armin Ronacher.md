---
layout: note
tags:
  - tech
  - individual
  - computer-science
  - rust
  - python
  - business
  - web
---

# Armin Ronacher


Armin Ronacher is an [[opensource]] developer, founder, speaker, blogger.

He is well known for his contributions towards [[Python]], and currently [[Rust]] ecosystem.

He also writes about [[entrepreneurship]] and [[opensource]].

Website: https://lucumr.pocoo.org/about/

GitHub: http://github.com/mitsuhiko

Twitter: http://twitter.com/mitsuhiko

Wikipedia: https://en.wikipedia.org/wiki/Armin_Ronacher
