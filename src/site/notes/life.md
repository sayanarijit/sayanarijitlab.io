---
layout: note
tags:
  - life
---

# Life

## Career

See [[career]].

## Wisdom

- https://wiki.nikitavoloboev.xyz/life

- Joe Rogan's Life Advice: https://youtu.be/u4taz6dfPQc

- "You should die naked. If you're still bothered by what other people will think of you, then you can't die just yet" ~ Ryuji Ayukawa.

- "There's no need to unnecessary new-ness. Most people crave for 'familiar'" ~ (TODO find source).

- "Don't lose sight of youself" ~ Everyone

- "When you ask a dumb question, you might get a smart, unexpected answer" ~ James Scott Bell

- "Meaningful life" is subjective. But "The meaning of life" is objective. Living a "meaningful life" is an aspect of living a good life, or of well-being, which some achieve and others don't, to differing degrees. If there is a "meaning of life", it would be a single meaning that applies to everyone.

- The purpose of life might be to ultimately figure out the purpose of life collectively.

- Randy Pausch's Last Lecture: https://www.cmu.edu/randyslecture/

- If there was an objective meaning of life, we wouldn't be free, we'd work like robots and compete with each other to fulfill that single meaning more than the others - [[Naval Ravikant]]: https://youtu.be/3qHkcs3kG44 Skip to 1:36:08

- If you are confused about how you want to live, focus on how you want to die.

- If you're doing something you like, wear something different, be unique, be creative, express yourself.

- If you want something, before aquiring it, ask yourself... Will it make you happier in life? Once you get it, would you really cherish it for the rest of your life, even if you don't get to show it off to other people?

- Develop a third eye for detecting distracting information. Be aware of the distractions. Keep a record of what distracted you throughout the day. Review weekly or monthly and optimize. Even relevant information can be irrelevant at the wrong time. Optimize how to handle them.

- Keep a record of what you own and why. Track every little thing. Review weekly or monthly to optimise.

- Be aware of when you are rushing but getting nowhere. It's usually when you want to just get things done, and when things turn out hairy, you feel annoyed instead of feeling curious and excited. Or when you are unable to get bored, or when you feel asleep while reading a book. Recognise, slow down and take back control. Hairy things should excite you, not annoy you. Everyone driving a car doesn't mean you are slower on your feet.

## We all are Hypocrites

- https://untoldstory.in/art/funny-indian-comics-brown-paper-bag/
- https://benwiederhake.github.io/randombias/
- https://www.scu.edu/ethics/ethics-resources/ethical-decision-making/consistency-and-ethics/

## Mental Health

### Dabrowski’s Theory and Existential Depression in Gifted Children and Adults

- https://www.davidsongifted.org/gifted-blog/dabrowskis-theory-and-existential-depression-in-gifted-children-and-adults/

## Ethics

- https://www.scu.edu/ethics/ethics-resources/a-framework-for-ethical-decision-making/

---

Also See:

- https://wiki.xxiivv.com/site/lifestyle.html
- My Journey (A.P.J. Abdul Kalam): https://www.goodreads.com/book/show/18371021-my-journey
