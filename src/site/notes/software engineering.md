---
layout: note
tags:
  - tech
  - computer-science
  - career
---

# Software Engineering

It is a discipline of [[computer science]] that also deals with solving problems.

## Software Development vs Software Engineering

While [[software development]] deals with solving problems by building software, software engineering does it by integrating existing software, often by [[programming]].

Although, most often, both software engineering and software development can be uses interchangeably, because in practice, the difference often gets blurry.

For example, when you build a backend for a website, you neither write the database logic, nor the http communication logic, not even the logic of the web framework that you used. You just integrate all the existing pieces together. So, are you a developer because you "built" a website or are you an engineer because you "integrated" all the existing pieces together? You will get different answers depending on who you ask.

However, the role of a Software Engineer include responsibilities way beyond just integrating software. They are also responsible for designing, deploying, analysing and maintaining software.

## Personal Opinions

- [[Configuration Environments and Secrets]]

## Resources

- System Design resources: https://github.com/InterviewReady/system-design-resources
- Software Engineering blogs: https://github.com/sumodirjo/engineering-blogs
- Awesome DB tools: https://github.com/mgramin/awesome-db-tools
- https://blog.pragmaticengineer.com/
- https://github.com/Alliedium/awesome-software-engineering
- https://backend.husseinnasser.com/
- https://www.freecodecamp.org/news/software-design/
- https://github.com/DovAmir/awesome-design-patterns
- https://github.com/mehdihadeli/awesome-software-architecture/
- https://github.com/simskij/awesome-software-architecture
- https://blog.pragmaticengineer.com/software-architecture-is-overrated/
- https://www.2ndquadrant.com/en/blog/partitioning-enhancements-in-postgresql-12/
- https://developers.cloudflare.com/
- https://www.worldql.com/posts/2021-08-worldql-scalable-minecraft/

---

Also See:

- [[security]]
