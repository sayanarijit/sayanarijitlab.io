---
layout: note
tags:
  - learning
  - writing
  - career
---

# Fiction

## Concepts

See [[concepts]].

## Learn

### How to find dialogues?

Can you recall witnessing a memorable incident?
Maybe someone saved a kid from an accident. Have you found yourself imagining how you'd respond if you were the victim or the hero?
What you'd say to each other?
If yes, do you recall placing yourself in someone else's shoe and imagine similar things?
If yes, write them down.

### 5 laws of indie publishing:

- You must think like a traditional publisher.
  - Be professional
  - Build a brand
  - Discipline
- You must write the best book you can everytime.
  - Learn more
    - Avoid getting influenced by stereotypes. Get feedback from a real person from the culture.
  - Read more
  - Write more
    - Write first. Edit, spellcheck, improve, revise later.
- Setup a system of quality control.
  - Your writing
  - Editing
  - Cover design
  - Marketing copy
  - Format the book
- Develop and work a marketing plan
- Repeat the process the rest of your life

### The writers pyramid

- Those who want to write, think they have a book inside
- Those who take initiative to learn writing (🙋 me)
- Those who write the first novel
- Those who write another
- Those who get published
- Those who get published again and again
- The real fortune

⚠️ Further notes are private. I highly recommend reading:

https://www.audible.in/pd/How-to-Write-Best-Selling-Fiction-Audiobook/1629976954

---

Also see:

- How to Build a Universe That Doesn’t Fall Apart Two Days Later: https://urbigenous.net/library/how_to_build.html
- Creating Strong Video Game Characters: https://youtu.be/4mgK2hL33Vw
- Storytelling Tools to Boost Your Indie Game's Narrative and Gameplay: https://youtu.be/8fXE-E1hjKk
- Learn about writing from this awesome anime: https://en.wikipedia.org/wiki/Shirobako
