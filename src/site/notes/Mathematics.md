---
layout: note
tags:
  - learning
  - math
---

# Mathematics

Wiki explains mathematics: https://en.wikipedia.org/wiki/Mathematics

## Resources

- https://ocw.mit.edu/courses/mathematics/
- https://github.com/rossant/awesome-math
- https://www.youtube.com/c/misterwootube/playlists
