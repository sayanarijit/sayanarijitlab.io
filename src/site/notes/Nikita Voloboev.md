---
layout: note
tags:
  - tech
  - individual
  - learning
---

# Nikita Voloboev

Nikita Voloboev's gigantic wiki https://wiki.nikitavoloboev.xyz, an absolute treasure filled with rabbitholes, is the main inspiration for my [[digital garden]]. He learns in public and shares so much knowledge, it's overwhelming.

You can ask him anything[^1].

Website: https://nikitavoloboev.xyz/

Wiki: https://wiki.nikitavoloboev.xyz/

GitHub: https://github.com/nikitavoloboev

Twitter: https://twitter.com/nikitavoloboev


[^1]: https://github.com/nikitavoloboev/ama
