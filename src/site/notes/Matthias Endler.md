---
layout: note
tags:
  - tech
  - individual
  - computer-science
  - learning
  - rust
  - business
---

# Matthias Endler

Matthias Endler, is an [[opensource]] maintainer, [[Rust]] consultant, speaker, blogger, and youtuber.

Website: https://endler.dev

GitHub: https://github.com/mre

Twitter: https://twitter.com/matthiasendler

Youtube: https://www.youtube.com/c/HelloRust

CO-founder of: https://codeprints.dev/

Calendar: https://calendly.com/matthias-endler
