---
layout: note
tags:
  - math
  - individual
  - computer-science
  - learning
---

# Bartosz Milewski


Bartosz Milewski teaches [[Mathematics]], particularly [[Category Theory]].

Website: https://bartoszmilewski.com/

Youtube: https://www.youtube.com/user/DrBartosz

GitHub: https://github.com/BartoszMilewski
